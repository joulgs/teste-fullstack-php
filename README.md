# 💻 Teste Full Stack PHP - Phidelis Tecnologia

[Layout](https://xd.adobe.com/view/5c3079dc-4d24-4d9c-8bb1-004437b6f107-0963/)

### Descrição Geral

O teste consiste em avaliar as habilidades do candidato na linguagem PHP observando lógica, domínio, padrões de projetos, organização, performance, velocidade na entrega, qualidade do código, análise e compreensão do escopo, assim como validar seus conhecimentos na integração entre interfaces front-end e back-end.

Será disponibilizado um layout em Adobe XD para orientação visual do que se espera no resultado final.  Entende-se que o candidato deve compreender todas as funcionalidades requisitadas a partir dele.

### Requisitos

- NÃO utilizar framework no back-end
- Utilizar um framework front-end
- Utilizar PHP 7.*
- PHP Orientado a Objetos
- Arquitetura MVC
- [PSRs](https://www.php-fig.org/psr/)
- HTML5
- CCS3
- JavaScript
- Git
- MySQL

### Diferenciais

- Utilizar Template Engine
- Utilizar Design Patterns
- Clean Code

### Tarefas

- [x] Autoloader
- [ ] MVC Funcional
- [ ] Router
- [ ] Template Base

### Instalando

- Primeiro rode um ``` npm install ``` para instalar as dependencias de frontend.