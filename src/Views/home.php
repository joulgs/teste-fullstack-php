<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Phidelis - PHP Dev</title>

    <link rel="stylesheet" href="../../node_modules/bootstrap/dist/css/bootstrap.css">
</head>
<body>
    
    <header>
        <nav class="navbar navbar-expand-lg bg-primary fixed-top ">
            <div class="container">
                <a href="https://www.phidelis.com.br/" class="navbrand">
                    <img src="/Views/assets/img/logo_Phidelis.png" alt="Logo Phidelis">
                </a>
            </div>
        </nav>
    </header>

    <section class="banner">
        #banner
    </section>

    <section class="">
        #viage conosco
    </section>

    <section class="">
        #viagens principais
        <?php
        
        $text = 'blablabla <br>';
        echo str_repeat($text, 100);


        ?>
    </section>

    <footer>
        <?= date('Y') ?>
    </footer>

</body>
</html>